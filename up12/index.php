<?php
/*
* Dado una ruta a un fichero, crear un directorio en donde
* copieis ese fichero añadiendo al final del mismo vuestra firma
*/
//variables
$ruta='';
$nombreDir='';
$okError=""; //donde guardo los posible fallos que se produzcan caso contrario muestro mensaje de procfeso terminado correctamente
if (isset($_GET['dato'])) {
    # pregunto si hay valor en get y si es asi lo guardo en la variable
    $ruta = $_GET['dato'];

    if (file_exists($ruta)){
        #compruebo que el dato pasado sea un fichero exstente
        $partes_ruta = pathinfo($ruta); //guardo la infarmacion del fichero -> para poder seleccionar el nombre del fichero que se ha pasado
        $nombreDir=strstr($ruta,'.',true); // guardo el nombre del fichero
        if (mkdir($nombreDir)) {
            #creo un directorio cuyo nombre es el nombre del fichero
            $nuevoFichero=$nombreDir.'/'.$partes_ruta['basename']; // uno el directrorio $nombreDir y el nombre del fichero para realizar el copy
            if (copy($ruta, $nuevoFichero)) {
                #realizo el copy del fichero en la nueva carpeta que se acaba de crear
                file_put_contents($nuevoFichero, 'Nelson Arias'); //escribo en el fichero la firma (nombre y apellido)
                $okError="Directorio y archivo creado y copiado correctamente<br>Firma incluida en nuevo fichero";
            }else{
                $okError='Error al Copiar fichero en nueva ruta';
            }
        }else{
            $okError='Error al crear directorio';
        }
    }else{
        $okError='El valor pasado debe ser el de un Fichero Existente';
    }
}else{
    $okError='No se ha pasado ningun dato por GET';
}
echo $okError;
