<?php
#variables
$resultado = ''; // donde se guarda lo que se muestra al usuario
if (isset($_POST['dato'])) {
    #compruebo pos para procesar el dato enviado
    $datoProcesa=strtolower(str_replace(" ","",$_POST['dato'])); //quito espacios en blanco para realizar comparaciones y paso a minusculas en caso de ser una frase palíndromo
    $aumento=(strlen($datoProcesa)%2==1)?1:0; //compruebo si tiene caracter central para sumarlo o no en la comparacion
    $mitadDato=round(strlen($datoProcesa) / 2,0,PHP_ROUND_HALF_DOWN); //saco la mitad del dato enviado en número
    $parte1=substr($datoProcesa,0,$mitadDato); //saco la primera parte del dato
    $parte2=strrev (substr($datoProcesa,$mitadDato+$aumento)); // saco la segunda mitad del dato añadiendo $aumento si es impar para que coja la segunda mitad correctamente con substr
    //y invierto el estrig para comprobar si son iguales
    if ($parte1==$parte2) {
        #comprueb
        $resultado = 'El ' . $_POST['dato'] . ' SI es palindromo o capicua';
    }else{
        $resultado = 'El ' . $_POST['dato'] . ' NO es palindromo ni capicua';
    }
}
include 'plantilla.html';
