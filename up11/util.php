<?php
function Milenialice($texto){
    #para el texto por diferentes replace
    $textoaux=str_replace('que','k',$texto);
    $textoaux=str_replace('porque','xq',$textoaux);
    $textoaux=str_replace('igual','=',$textoaux);
    $textoaux=str_replace('¿','',$textoaux);
    $textoaux=str_replace('¡','',$textoaux);
    #cada replace se podria poner poner en una funcion diferente y ser llamada
    # para esto defino una funcion que ponga en mayuscula cada letra par de la frase que se envia
    $textoaux = cambiaMayus($textoaux);
    return $textoaux; //retorno el valor una vez terminados los replace y la funcion cambiaMayus
}
function cambiaMayus($txtaux){
for ($i=0; $i < strlen($txtaux); $i++) { 
    if ($i%2==0) {
        $txtaux[$i]=strtoupper($txtaux[$i]);
    }
}
return $txtaux;
}