<?php
//variables
$resultado='';
if (isset($_POST['dato'])) {
    # compruebo que se ha enviado algo por el formulario
    $palabra=strtolower($_POST['dato']);
    $archivo = $_FILES['archivo'];
    $archivoAbierto = fopen($archivo['tmp_name'], 'r'); // lo abro en modo solo lectura (r)
    $conteo = 0; //contador para mostrar el numero de palabras que se repiten
    while ($linea = fgets($archivoAbierto)) {
        #recorro cada linea entera fgets guardandola en $linea para procesarla
        $lineaPartes=explode(" ",$linea); //separo las palabras por espacios
        foreach ($lineaPartes as $word) {
            # recorro cada palabra para comprovar si es igual a la que se busca
            if (strpos(strtolower($word),$palabra)!==false) {
                # strpos busco si la palabra a buscar esta dentro de cada palabra del array
                # esto es porque al separar puede estar dentro del array la palabra seguido de una como o signo de interrogacion o puntuacion (, ; .)etc
                $conteo++;
            }
        }
    }
    fclose($archivoAbierto);
    $resultado = "La palabra $palabra se repite: $conteo veces";
    #comentado una forma mas simple y con menos codigo para conseguir el mismo resultado
    /*$text= file_get_contents($archivo['tmp_name']);
    $resultado = "La palabra $palabra se repite: ".substr_count(strtolower($text), $palabra).' veces';
    */
}

include('plantilla.html');