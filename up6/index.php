<?php
$color = ['blanco', 'verde', 'rojo'];
$colorComa=''; // varible para guardar los valores separados por comas
foreach($color as $colores): 
    #recorro y guardo el valor mas una coma
    $colorComa.=$colores.',';
endforeach;
echo rtrim($colorComa,','); //quito la ultima coma para que se vea correctamente

sort($color); //ordeno el array
echo "<ul>";
foreach($color as $colores): 
    #lo imprimo entre li para que se forme la lista
    echo "<li>".$colores.'</li>';
endforeach;
echo "</ul>";
