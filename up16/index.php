<?php
#variables
$resultado=''; //donde guardo el resultado de los procesos y que se muestra al usuario
#lo divido en dos arrays se podria hacer en uno solo usando clave valor pero se perderia la ñ al escribir el array
#se podria hacer con array clave valor pero seria un poco mas de codigo y por simplpificar alo visto en clase
$paices=['España', 'Polonia', 'Costa Rica']; //array de paises
$saludos=['¡Bienvenido!', 'Witamy!', '¡Pura Vida!']; //array de saludos
if (isset($_POST['pais'])) {
    #compruebo si se hace post y actualizo el valor tanto de la cookie como del usado en el select
    $paisSeleccionado = $_POST['pais'];
    setcookie("pais", $paisSeleccionado);
} else {
    #si no hay post compruebo si hay o no cookie
    if (isset($_COOKIE['pais'])) {
        #si hay cookie recupero el valor que contiene
        $paisSeleccionado = $_COOKIE['pais'];
    } else {
        #si es la primera vez que entra en el index doy por defecto un valor
        #comento el ramdom para dejarlo como visto en clase (ramdom lo pongo al interpretar el ejercicio)
        //$paisSeleccionado = rand(0,2);
        $paisSeleccionado = 0;
    }
}

$resultado = $saludos[$paisSeleccionado];

include('plantilla.html');
