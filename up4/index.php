<?php
/*varibles a usar */
$nombreArchivo=''; //donde se guardara el valor pasado por get
$nombre_Mod=""; // donde se guardara el valor de $nombreArchivo procesado
if (isset($_GET['dato'])) {
    # pregunto si hay valor en get y si es asi lo guardo en la variable
    $nombreArchivo= strtolower($_GET['dato']);//pongo el string en minusculas para poder comprobar con strpos
}

if (strpos($nombreArchivo,".exe")>-1){
    #busco el substring en la cadena y compruebo si es mayor a -1 (si es mayor a -1 a encontrado el substring)
    $nombre_Mod=strtoupper($nombreArchivo); //pongo el string en mayusculas
}else if (strpos($nombreArchivo,".bd")>-1) {
    #si el anterior if da false en este compruebo si tiene el substring .bd
    $nombre_Mod=strtolower($nombreArchivo); // si lo tiene pongo el string en minusculas
}else{
    #si no tiene ninguna es que es un texto sin mas o que la extencion no es la que se pide
    $nombre_Mod='Nombre de fichero incorrecto'; //guardo un texto descriptivo para mostrar
}
#imprimo el contenido de la varible que tiene el resultado de procesar el string que se paso
echo $nombre_Mod;