<?php
$sumaTotal=0;
if (isset($_GET['dato'])) {
    # compruebo si se ha pasado datos, doy por echo que el usuario pase un numeros separados por comas
    $numeros=explode(',',$_GET['dato']); //guardo el dato pasandolo por explode para que se combierta en un array de valores
    $sumaTotal = sumaValores($numeros); //llamo a la funcion y el resultado lo guardo en la variable a mostrar
    echo "El total es $sumaTotal";
}


function sumaValores($array){
    #funcion que dado un array lo recorre y suma sus valores
    $total = 0;
    foreach ($array as $value) {
        $total += intval($value);
    }
    return $total; //devuelvo el total de la suma de los valores de un array
}
