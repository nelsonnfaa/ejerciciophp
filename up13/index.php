<?php
$archivo='archivo.txt'; // ruta y nombre del archivo que se va ha procesar en este caso solo nombre porque esta en la misma carpeta que el php
$archivoAbierto = fopen($archivo, 'r'); // lo abro en modo solo lectura (r)
$i=1; //contador para mostrar el numero de linea en que se esta
while ($linea = fgets($archivoAbierto)) {
    #recorro cada linea entera fgets guardandola en $linea para procesarla
    echo "LINEA $i: ".$linea ."   ==> el numero de palabras son: ".str_word_count($linea).'<br/>';// la linea y el numero de linea 
    //con str_word_count cuento el numero de palabras de cada linea
    $i++;
}
fclose($archivoAbierto);
